EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Orvet - si7021 version"
Date "2019-02-12"
Rev ""
Comp "Crafting Labs"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L orvet-rescue:HT7333-Crafting_Labs U1
U 1 1 5C60420D
P 4950 2900
F 0 "U1" H 4632 3013 60  0000 C CNN
F 1 "HT7333" H 4632 3119 60  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 4950 2900 60  0001 C CNN
F 3 "" H 4950 2900 60  0000 C CNN
	1    4950 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C1
U 1 1 5C604377
P 4800 3100
F 0 "C1" V 5055 3100 50  0000 C CNN
F 1 "CP" V 4964 3100 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4838 2950 50  0001 C CNN
F 3 "~" H 4800 3100 50  0001 C CNN
	1    4800 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5000 3600 4950 3600
Wire Wire Line
	5000 3450 4550 3450
Wire Wire Line
	4550 3300 4650 3300
Wire Wire Line
	4950 3600 4950 3100
Connection ~ 4950 3600
Wire Wire Line
	4950 3600 4550 3600
Wire Wire Line
	4650 3100 4650 3300
Connection ~ 4650 3300
$Comp
L orvet-rescue:si7021-Crafting_Labs U3
U 1 1 5C609751
P 6900 2200
F 0 "U3" H 7128 2221 50  0000 L CNN
F 1 "si7021" H 7128 2130 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 6500 1900 50  0001 C CNN
F 3 "" H 6500 1900 50  0001 C CNN
	1    6900 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2750 6100 2750
Wire Wire Line
	4650 3300 5000 3300
Wire Wire Line
	6600 2250 5900 2250
Wire Wire Line
	5900 2350 6600 2350
NoConn ~ 4850 2150
NoConn ~ 5900 2450
NoConn ~ 5900 2650
$Comp
L Device:LED D1
U 1 1 5E4CCFE0
P 2100 2400
F 0 "D1" H 2093 2145 50  0000 C CNN
F 1 "LED" H 2093 2236 50  0000 C CNN
F 2 "LED_SMD:LED_1W_3W_R8" H 2100 2400 50  0001 C CNN
F 3 "~" H 2100 2400 50  0001 C CNN
	1    2100 2400
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 5E4CD3EE
P 2100 3550
F 0 "D2" H 2093 3295 50  0000 C CNN
F 1 "LED" H 2093 3386 50  0000 C CNN
F 2 "LED_SMD:LED_1W_3W_R8" H 2100 3550 50  0001 C CNN
F 3 "~" H 2100 3550 50  0001 C CNN
	1    2100 3550
	-1   0    0    1   
$EndComp
Text GLabel 1750 3550 0    50   Input ~ 0
+5V
$Comp
L Transistor_BJT:TIP122 Q1
U 1 1 5E4D6696
P 3300 2400
F 0 "Q1" H 3507 2446 50  0000 L CNN
F 1 "TIP122" H 3507 2355 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 3500 2325 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 3300 2400 50  0001 L CNN
	1    3300 2400
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP122 Q2
U 1 1 5E4D8290
P 3300 3550
F 0 "Q2" H 3507 3596 50  0000 L CNN
F 1 "TIP122" H 3507 3505 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabUp" H 3500 3475 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 3300 3550 50  0001 L CNN
	1    3300 3550
	1    0    0    -1  
$EndComp
Text GLabel 3400 3900 2    50   Input ~ 0
GND
Wire Wire Line
	3400 2600 3400 2750
Text GLabel 1750 2400 0    50   Input ~ 0
+5V
Text GLabel 4650 2450 0    50   Input ~ 0
GPIO14
Text GLabel 4650 2550 0    50   Input ~ 0
GPIO12
$Comp
L orvet-rescue:ESP12-Crafting_Labs U2
U 1 1 5C60F50A
P 5550 3650
F 0 "U2" H 5375 5537 60  0000 C CNN
F 1 "ESP12" H 5375 5431 60  0000 C CNN
F 2 "crafting-labs:ESP12-breakout_board" H 5550 3650 60  0001 C CNN
F 3 "" H 5550 3650 60  0000 C CNN
	1    5550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2450 4650 2450
Wire Wire Line
	4850 2550 4650 2550
NoConn ~ 4850 2650
Text GLabel 1750 1950 0    50   Input ~ 0
GPIO14
Wire Wire Line
	2250 3550 2500 3550
Wire Wire Line
	3400 3350 3400 3150
Wire Wire Line
	3400 3750 3400 3900
Text GLabel 1750 3150 0    50   Input ~ 0
GPIO12
Text GLabel 3400 2750 2    50   Input ~ 0
GND
Wire Wire Line
	2500 2400 2250 2400
Text GLabel 4650 2250 0    50   Input ~ 0
+3,3V
Text GLabel 6500 2150 0    50   Input ~ 0
GND
Text GLabel 7050 3900 3    50   Input ~ 0
GND
Text GLabel 7600 3150 2    50   Input ~ 0
+5V
Text GLabel 4550 3300 0    50   Input ~ 0
+3,3V
Text GLabel 4550 3450 0    50   Input ~ 0
+5V
Text GLabel 4550 3600 0    50   Input ~ 0
GND
Text GLabel 4650 2750 0    50   Input ~ 0
+3,3V
Text GLabel 6100 2750 2    50   Input ~ 0
GND
Text GLabel 6500 2050 0    50   Input ~ 0
+3,3V
Wire Wire Line
	6600 2150 6500 2150
Text GLabel 5700 1400 0    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E537147
P 5900 1400
F 0 "#FLG0101" H 5900 1475 50  0001 C CNN
F 1 "PWR_FLAG" V 5900 1528 50  0000 L CNN
F 2 "" H 5900 1400 50  0001 C CNN
F 3 "~" H 5900 1400 50  0001 C CNN
	1    5900 1400
	0    1    1    0   
$EndComp
Text GLabel 5700 1550 0    50   Input ~ 0
+5V
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E537911
P 5900 1550
F 0 "#FLG0102" H 5900 1625 50  0001 C CNN
F 1 "PWR_FLAG" V 5900 1678 50  0000 L CNN
F 2 "" H 5900 1550 50  0001 C CNN
F 3 "~" H 5900 1550 50  0001 C CNN
	1    5900 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 1550 5900 1550
Wire Wire Line
	5700 1400 5900 1400
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5E543A5D
P 7450 1250
F 0 "J3" V 7512 1394 50  0000 L CNN
F 1 "Conn_01x03_Male" V 7603 1394 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7450 1250 50  0001 C CNN
F 3 "~" H 7450 1250 50  0001 C CNN
	1    7450 1250
	0    1    1    0   
$EndComp
Text GLabel 7550 1450 3    50   Input ~ 0
GND
Text GLabel 7450 1450 3    50   Input ~ 0
RXD
Text GLabel 7350 1450 3    50   Input ~ 0
TXD
Text GLabel 5900 2050 2    50   Input ~ 0
TXD
Text GLabel 5900 2150 2    50   Input ~ 0
RXD
Wire Wire Line
	6600 2050 6500 2050
Wire Wire Line
	4850 2750 4650 2750
Text GLabel 4650 2050 0    50   Input ~ 0
RST
Text GLabel 4650 2350 0    50   Input ~ 0
GPIO16
Wire Wire Line
	4650 2350 4850 2350
Wire Wire Line
	4850 2050 4650 2050
Wire Wire Line
	4850 2250 4650 2250
Text GLabel 4500 1100 2    50   Input ~ 0
RST
Text GLabel 4500 1500 2    50   Input ~ 0
GPIO16
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5E55D11E
P 4700 1350
F 0 "J2" H 4672 1232 50  0000 R CNN
F 1 "Conn_01x02_Male" H 4672 1323 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4700 1350 50  0001 C CNN
F 3 "~" H 4700 1350 50  0001 C CNN
	1    4700 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 1100 4350 1250
Wire Wire Line
	4350 1250 4500 1250
Wire Wire Line
	4350 1100 4500 1100
Wire Wire Line
	4500 1350 4350 1350
Wire Wire Line
	4350 1350 4350 1500
Wire Wire Line
	4350 1500 4500 1500
$Comp
L pspice:R R2
U 1 1 5E4F69B9
P 2750 2400
F 0 "R2" V 2955 2400 50  0000 C CNN
F 1 "R" V 2864 2400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 2750 2400 50  0001 C CNN
F 3 "~" H 2750 2400 50  0001 C CNN
	1    2750 2400
	0    -1   -1   0   
$EndComp
$Comp
L pspice:R R1
U 1 1 5E4F78AA
P 2750 1950
F 0 "R1" V 2545 1950 50  0000 C CNN
F 1 "R" V 2636 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 2750 1950 50  0001 C CNN
F 3 "~" H 2750 1950 50  0001 C CNN
	1    2750 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2200 3400 1950
Wire Wire Line
	3100 2400 3000 2400
Wire Wire Line
	1950 2400 1750 2400
Wire Wire Line
	3400 1950 3000 1950
Wire Wire Line
	1750 1950 2500 1950
$Comp
L pspice:R R4
U 1 1 5E532493
P 2750 3550
F 0 "R4" V 2955 3550 50  0000 C CNN
F 1 "R" V 2864 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 2750 3550 50  0001 C CNN
F 3 "~" H 2750 3550 50  0001 C CNN
	1    2750 3550
	0    -1   -1   0   
$EndComp
$Comp
L pspice:R R3
U 1 1 5E5384F4
P 2750 3150
F 0 "R3" V 2955 3150 50  0000 C CNN
F 1 "R" V 2864 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 2750 3150 50  0001 C CNN
F 3 "~" H 2750 3150 50  0001 C CNN
	1    2750 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 3150 3000 3150
Wire Wire Line
	2500 3150 1750 3150
Wire Wire Line
	1750 3550 1950 3550
Wire Wire Line
	3100 3550 3000 3550
$Comp
L LED:WS2812B D3
U 1 1 5E53E569
P 5750 4350
F 0 "D3" H 6094 4396 50  0000 L CNN
F 1 "WS2812B" H 6094 4305 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5800 4050 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5850 3975 50  0001 L TNN
	1    5750 4350
	1    0    0    -1  
$EndComp
NoConn ~ 6050 4350
Text GLabel 5150 4350 0    50   Input ~ 0
GPIO2
Text GLabel 6100 2550 2    50   Input ~ 0
GPIO2
Wire Wire Line
	6100 2550 5900 2550
Wire Wire Line
	5450 4350 5150 4350
Text GLabel 5750 4850 0    50   Input ~ 0
GND
Text GLabel 5750 3850 0    50   Input ~ 0
+5V
Wire Wire Line
	5750 3850 5750 4050
Wire Wire Line
	5750 4650 5750 4850
$Comp
L Connector:USB_OTG J1
U 1 1 5E55DAD0
P 7050 3350
F 0 "J1" H 7107 3817 50  0000 C CNN
F 1 "USB_OTG" H 7107 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7200 3300 50  0001 C CNN
F 3 " ~" H 7200 3300 50  0001 C CNN
	1    7050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3750 7050 3900
Wire Wire Line
	7350 3150 7600 3150
NoConn ~ 7350 3350
NoConn ~ 7350 3450
NoConn ~ 7350 3550
NoConn ~ 6950 3750
$EndSCHEMATC
